#ifndef __WAVE_FORM_H__
#define __WAVE_FORM_H__
#include <stm32f10x.h>

void sin_wave(void)	;
void triangle_wave(void);
void zigzag_wave(void);
#endif


