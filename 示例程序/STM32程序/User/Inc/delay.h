 /*
  ******************************************************************************
  * @文件名		：  delay.h
  * @作者		：	JCY
  * @版本		：  V1.0.0
  * @时间		：  2012-11-13
  * @概要		：	这个文件提供了在应用程序中使用的必要的延时函数
  *          	
  ******************************************************************************
  * @注意
  *
  * 本延时函数是在仿真的情况下测试得到，经过实际验证延时还算精确。
  * 晶振：8MHZ，AHB：72MHZ，APB2 :72MHZ，AP1：36HZ
  *
  ******************************************************************************
  */
#ifndef __DELAY_NMS__
#define __DELAY_NMS__
#include <stm32f10x.h>

typedef struct delay_fun_t
{
	
	void (* init)(void);
	void (* nus)(unsigned int nus);
	void (* nms)(unsigned int nms);
	
}delay_fun_t;

extern delay_fun_t delay;

#endif	//结束__DELAY_NMS__

