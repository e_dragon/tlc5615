#ifndef __USART_H__
#define	__USART_H__

#include "stm32f10x.h"
#include <stdio.h>

void uart1_init(void);
uint8_t  uart1_rec_data(void);
#endif /* __USART_H */
