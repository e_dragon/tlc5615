 /*
  * 水星电子科技
  ******************************************************************************
  * @文件名		：  TCL5615.c
  * @作者		：	JCY
  * @版本		：  V1.0.0
  * @时间		：  2013-02-05
  * @概要		：	在此文件中包含了对TCL5615的相关操作接口
  *          	
  ******************************************************************************
  * @注意
  *STM32F103C8与TCL5615连接如下：
  *STM32			TCL5615		
  *PA2		->		CS			
  *PA5		->		SCLK
  *PA6		->		DOUT
  *PA7		->		DIN
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stdio.h"
#include "TCL5615.h"
#include "delay.h"

#define  CS_H()  GPIO_SetBits(GPIOA,GPIO_Pin_2)
#define  CS_L()  GPIO_ResetBits(GPIOA,GPIO_Pin_2)
static void TCL5615_SPI_pin_init(void);
void TCL5615_init(void);
void write_data_to_TCL5615(u16 data);


static void TCL5615_SPI_pin_init(void)
{         
	SPI_InitTypeDef  SPI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1|RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO, ENABLE);

	/* Configure SPI1 pins: SCK, MISO and MOSI */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	
	
	//配置片选引脚
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //上拉输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_SetBits(GPIOA,GPIO_Pin_2); 
	

	/* SPI1 configuration */
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;                
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;           
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;              
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;       
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;                
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;  //SPI时钟：36M         
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;        
	SPI_InitStructure.SPI_CRCPolynomial = 7;           
	SPI_Init(SPI1, &SPI_InitStructure);             

	SPI_Cmd(SPI1, ENABLE);             
}  







     

void TCL5615_init(void)
{        
	//SPI通信相关引脚配置
	TCL5615_SPI_pin_init();
}



void write_data_to_TCL5615(u16  data)
{
	
	u16 da_value =  data<<2;
	
	CS_L();
	delay.nus(2);
	SPI_I2S_SendData(SPI1, (da_value>>8)&0XFF); 
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);//等待发送结束 
	SPI_I2S_SendData(SPI1, da_value&0XFF); 
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);//等待发送结束 
	delay.nus(2);
	CS_H();
	delay.nus(2);
}


                              

