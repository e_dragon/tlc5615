#include "uart.h"
#include "stm32f10x.h"

#include <stdarg.h>
static void uart1_interrput_init(void);
void uart1_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	/* config USART1 clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
	
	/* USART1 GPIO config */
	/* Configure USART1 Tx (PA.09) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);    
	/* Configure USART1 Rx (PA.10) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	  
	/* USART1 mode config */
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure); 
	USART_Cmd(USART1, ENABLE);
	uart1_interrput_init();
}




/*
----------------------------------------------------------------------------------------uart1_interrput_init
函数名		：
				uart1_interrput_init
功能		：
				uart1中断初始化
输入参数	：
				无
输出参数	：
				无
返回值		：
				无
其他		：
				无
*/

static void uart1_interrput_init(void)
{
  	NVIC_InitTypeDef NVIC_InitStructure;
	
  	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	
  	/* Enable the RTC Interrupt */
  	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn; // 
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  // 主优先级为0； 
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;	// 从优先级为1；
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;	  // 中断使能 ；
  	NVIC_Init(&NVIC_InitStructure);

	//USART_ITConfig(USART1, USART_IT_TC, ENABLE);//发送完成中断
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); //接收完成中断
}




uint8_t  uart1_rec_data(void)
{
	uint8_t rec_data;
	while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
	rec_data = (unsigned char)USART_ReceiveData(USART1);
	return rec_data;
}

int fputc(int ch, FILE *f)
{
	USART_SendData(USART1, (unsigned char) ch);
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	
	return (ch);
}

