#include <stm32f10x.h>

#include "var_decl.h"//jcy add
/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{

}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
 


/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
	while (1)
	{
	}
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
*/

void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
	while (1)
	{

	}
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
	while (1)
	{

	}
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
	while (1)
	{

	}
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
	while (1)
	{

	}
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
//OS_CPU_SR cpu_sr;
void SysTick_Handler(void)
{
}


/*******************************************************************************
* Function Name  : RTC_IRQHandler
* Description    : This function handles RTC global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void RTC_IRQHandler(void)
{

}


void USART1_IRQHandler(void)
{
	
	if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET)
	{
		wave_select = uart1_rec_data();
		switch(wave_select)
		{
			case '0':
				printf("正弦波\r\n");
				break;
			case '1':
				printf("三角波\r\n");
				break;			
			case '2':
				printf("锯齿波\r\n");
				break;	
			default :
				printf("WARNNING:接收到非法数据\r\n");
				break;
		}
		printf("USART1_IRQHandler: 接收到的数据为%d\r\n",wave_select);
		
	}
}


void USART3_IRQHandler(void)
{

}
/*******************************************************************************
* Function Name  : SDIO_IRQHandler
* Description    : This function handles SDIO global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SDIO_IRQHandler(void)
{

}




/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{

}*/

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
