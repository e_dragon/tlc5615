/*****TLC5615驱动程序*************/
/***基于AT89S52的程序*************/
#include<reg52.h>
#include<intrins.h>
#define uchar unsigned char
#define uint unsigned int
sbit DIN=P1^2;
sbit CLK=P1^1;
sbit CS=P1^0;
uchar code table[]={0x6f,0x6a,0x67,0x60,0x5f,0x5c,
0x5a,0x57,0x54,0x50,0x4f,0x4c,0x4a,0x47,0x44,0x40,
0x3f,0x3c,0x3a,0x37,0x34,0x30,0x2f,0x2c,0x2a,0x27,
0x24,0x20,0x1f,0x1c,0x1a,0x17,0x14,0x10,0x0f,0x0c,
0x0a,0x07,0x04,0x00};
/****延时us函数******/
void delay(uint t)
{
	uint x,y;
	for(x=t;x>0;x--)
		for(y=110;y>0;y--);
}

/******	输出数据函数***********/
void DA5615(uint j)
{
	uint i;
	uchar temp=table[j];
	CLK=0;
	CS=0;
	for(i=0;i<12;i++)
	{
		temp=temp<<1;
		DIN=CY;
		CLK=1;
		_nop_();
		CLK=0;
	}
	CS=1;

}
/***********主函数***************/
void main()
{
	uint num;
	while(1)
	{

	for(num=0;num<24;num++)
	{
	   DA5615(num);
	   delay(500);

	}
	for(num=24;num>0;num--)
	{
		DA5615(num);
		delay(500);
	}
	}	
}