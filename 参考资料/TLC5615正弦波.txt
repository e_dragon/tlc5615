
#include <reg52.h>
#include <math.h>
#include <intrins.h>
#define  uint unsigned int
#define  uchar unsigned char



#define  PI 3.14
#define DataNum 128 //定义一个周期的点数

#define INBUF_LEN 6 //数据长度
unsigned char inbuf1[INBUF_LEN]={'0','0','0','0','0','0'};//发送缓冲区

void init_serialcomm( void )
{
SCON = 0x50 ; //SCON: serail mode 1, 8-bit UART, enable ucvr
T2CON=0x30;
TH2=0x00;
TL2=0x00;
RCAP2H=0xFF;
RCAP2L=0xDC;
TR2=1;
}
//向串口发送一个字符
void send_char_com( unsigned char ch)
{
SBUF=ch;
while (TI== 0 );
TI= 0 ;
}
//向串口发送一个字符串，strlen 为该字符串长度
void send_string_com( unsigned char *str, unsigned int strlen)
{
unsigned int k= 0 ;
do
{
send_char_com(*(str + k));
k++;
} while (k < strlen);
}

unsigned char code table[] = {0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90,0x88,0x83,0xc6,0xa1,0x86,0x8e};//共阳数码管显示0~F
unsigned char code dis_8[12]={'0','1','2','3','4','5','6','7','8','9',' ','-'};
sbit CS=P1^0; //定义片选信号IO口
sbit SCLK=P1^1; //定义时钟信号IO口
sbit DIN=P1^2; //定义数据输入IO口
sbit p24=P2^4;
sbit p25=P2^5;
sbit p26=P2^6;
sbit p27=P2^7;
uint temp;
void delay(uint k)		
{						
uint i,j;					
for(i=0;i<k;i++){		
for(j=0;j<121;j++)		
{;}}					
}			



//实现DA转换
void DAConvert(uint Data){
  uchar i;
  Data<<=6;
  CS=0;
  SCLK=0;
  for (i=0;i<12;i++){
    DIN=(bit)(Data&0x8000);
    SCLK=1;
    Data<<=1;
    SCLK=0;
  }
  CS=1;
  SCLK=0;
  for (i=0;i<12;i++);
}
uint GetSinData(uchar x){
  uint SinData;
  SinData=((sin(2*PI*x/DataNum)+1)/2.0)*1024;
  return SinData;
}

 //定时器0中断  扫描显示
void KeyAndDis_Time0(void) interrupt 1 using 2
{
  TH0=0xdc;
	TL0=0x00;				//设定时值为20000us（20ms)


    P0= table[ temp/1000];	
	p34=0;				
	delay(1);
	p34=1;	

  P0= table[ (temp%1000)/100];	
	p35=0;				
	delay(1);
	p35=1;				
	P0= table[ (temp%100)/10];	
p36=0;				
	delay(1);
	p36=1;				
	P0= table[ temp%10];	
p37=0;				
	delay(1);
	p37=1;			



}







//主程序
main(){
   uint i;

 init_serialcomm(); //初始化串口
  EA = 1;					//允许CPU中断
	ET0 = 1; 				//定时器0中断打开
	TMOD = 0x1;			//设定时器0为模式1，16位模式
	TH0=0xdc;
	TL0=0x00;				//设定时值为10MS
	TR0 = 1;

while(1){
for(i=0;i<DataNum;i++){ 

DAConvert(GetSinData(i));//输出正弦波
 temp=GetSinData(i);
inbuf1[0]=dis_8[temp/1000];
inbuf1[1]=dis_8[(temp%1000)/100];
 inbuf1[2]=dis_8[(temp%100)/10];
 inbuf1[3]=dis_8[temp%10];
inbuf1[4]=0x0d;
  inbuf1[5]=0x0a;
send_string_com(inbuf1,INBUF_LEN);
	   }

  }
}



